# Dynamic world

## Cyclical activities - done
### Main searching cycle
This cycle is used as foundation for all NPC characters ( except not agressive NPCs like traders and etc. ) . 
Some specific mobs may have modified or reduced cycles.
<img width="400" alt="banka" src="images/Cycles/1.PNG">

### Spawned mobs
Players can spawn they own mobs with tower's shop.<br/> After their appearence they will follow MSC ( main searching cycle ) in a limited range aroung the tower.<br/>
Mob's owner may direct them to one of the enemies tower.<br/> During their way to the enemy tower they use MSC with following modification ( Random roam -> Directed walk ).<br/>
<img width="400" alt="banka" src="images/Cycles/2.PNG">

## Cyclical activities - in progress
### Traders
#### Trading machine
 - Each player has their own trading machine in their starting bioms.
 - Because starting areas are closed for the enemies, each trading machine is private.
 - It means that unique content of machine is available for the biom's owner only.
 - The only cycle they have is that they change the entire assortment once per ( not defined for now ) time.

#### Event trader
 - The event trader may appear during the game. Сorresponding information is going to be announce it in event table.
 - Event trader dont move on his own but change his location once he made a deal or he spent 1 min in the same place without any interactions.
 - After several cycles he disappears and event ends.
<img width="400" alt="banka" src="images/Cycles/3.PNG">

### Boss
 - Boss may appear ( like an event traders ) whenever but not during the first 2 min of the game.
 - Boss do not attack any other NPC characters except players and their mobs.
 - The only way to end boss event is to kill the boss.
<img width="400" alt="banka" src="images/Cycles/4.PNG">

### Traps
 - Traps cycle is as simple as it should be.
 - Traps can trigger both players and mobs.
 - Traps can not be destroyed.
<img width="400" alt="banka" src="images/Cycles/13.PNG">


## Example of MSC triggering
At the following screen you may see
 - MSC ( Random roaming => Found enemy)
 - Triggering the trap

<img width="400" alt="banka" src="images/Cycles/5.PNG">
=======>
<img width="400" alt="banka" src="images/Cycles/6.PNG">

Example of MSC with other mob

<img width="400" alt="banka" src="images/Cycles/8.PNG">
=======>
<img width="400" alt="banka" src="images/Cycles/11.PNG">


One more example of MSC

<img width="400" alt="banka" src="images/Cycles/22.PNG">
=======>
<img width="400" alt="banka" src="images/Cycles/23.PNG">
