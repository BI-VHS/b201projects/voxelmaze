## Login menu

<img width="1000" alt="portfolio_view" src="images/doc3/login_menu.png">

1. List of visible rooms
2. Button, when pressed, the player will connect to the room. Connection is possible if the room is open and the player limit has not been reached. contains basic information about the room: name of the room, number of players in and is the room open.
3. Buttons, which allows you to interact with room list:
    - Input field for choosing a nickname.
    - Room creation button. Also allows you to hide a room in the list of rooms. In this case, it will be possible to connect to the room randomly, or using the name of the room.
    - Next button allows you to connect to the room using its name.
    - This button will randomly connect to the room.
    - This button will return you to the main menu.

<br/> After connecting to the room, you will be transferred to the character selection menu

## Character selection menu

<img width="1000" alt="portfolio_view" src="images/doc3/select_hero.png">

Here you can choose one of the four available characters.
<br/> The game will start when all players press "Ready" button and then the host press "Start" button.

## Game

### Heroes

#### Alice

<img width="200" alt="portfolio_view" src="images/doc3/Alice_hero_ref.png">

The heroine of the deep sea.
<br/>Armed with an automatic blaster. 
- Average damage
- High rate of fire
- Large scatter of bullets

#### Daniel

<img width="250" alt="portfolio_view" src="images/doc3/Daniel_hero_ref.png">

The latest model of combat robot. 
<br/>Equipped with a high-precision laser
- High precision and firing range 
- Deals damage over time

#### Bob

<img width="200" alt="portfolio_view" src="images/doc3/Bob_hero_ref.png">

Desert and tombs explorer.
<br/>Armed with a double-barreled shotgun.
- Low rate of fire
- High damage up close
- Large scatter

#### Elly

<img width="200" alt="portfolio_view" src="images/doc3/Eli_bot.png">

Lover of cakes and muffins.
<br/>Uses long range blaster rifle.
- High damage
- High firing range
- Low rate of fire
- Сhance of ricochet when hitting an enemy

### Location

The scene is a procedurally generated maze divided into 9 zones. 
<br/>There is a tower in each zone: the main tower in the central and auxiliary towers in the other eight.

<img width="1000" alt="portfolio_view" src="images/doc3/main_tower.png">

<img width="1000" alt="portfolio_view" src="images/doc3/auxiliary_tower.png">

<br/>Players appear in the corners of the maze, each in their own zone. 
<br/>At the beginning, the maze is hidden from players. It will gradually open as you explore.
<br/>Players should capture auxiliary towers. After a player captures a tower, the entire area around the tower will gradually change depending on the player who captured it.

### Goal of the game

Each player needs to capture the central tower before the rest of the players. The only way to do it is capturing and upgrading auxiliary towers.
<br/>Main tower capture statistics are displayed in the center of the screen

<img width="1000" alt="portfolio_view" src="images/doc3/capture_bars.png">

### Mobs

There are 5 types of mobs in the game:
- Skeletons

![](images/doc3/skeleton.gif)

- Robots

![](images/doc3/robot.gif)

- Sea Hourses

![](images/doc3/sea_hourse.gif)

- Mummies

![](images/doc3/mummy.gif)

- Jelly Slimes

![](images/doc3/jelly_slime.gif)

Unlike friendly ones, enemy mobs attack you immediately, as soon as you get close to them. They'll lose you if you run far enough.

Friendly mobs will not attack you. They can also be recruited into the squad. After that they will follow you. To do this, hold down the right mouse button and swipe over a friendly mob.

### User Interface

#### Main

<img width="1000" alt="portfolio_view" src="images/doc3/ui_main.png">

1. 
    - Cube button opens game menu
    - Map button opens map
2. Characters HP bar
3. Score counter
4. Left and right arrows ar buttons. Press each of them to open FPS counter.
5. Capture bars. Represents main tower capturing progress.
6. Chat area. Click on it to open chat input field. Then press Enter to send your message or click anywhere else to close it.

#### Death screen

<img width="1000" alt="portfolio_view" src="images/doc3/ui_death.png">

#### Victory screen

<img width="1000" alt="portfolio_view" src="images/doc3/ui_victory.png">

#### Map

<img width="1000" alt="portfolio_view" src="images/doc3/ui_map.png">

1. Map area. Area will be open together with the labyrinth. You can drag it with mose.
2. List of buttons. Each button represents tower. Pressing the button will transfer the map to the selected tower and, if its possible, open its characteristics window.
3. Tower characteristic window. It shows current tower level and capture status. It also allows to interact with tower with help of three burrons:
    - Deffence button. Stops capturing for 5 seconds. It has cooldown 15 seconds
    - Tower level up button. 5 is max possible tower level. To level up, you need score points. Each next level costs more than previos one.
    - Button to create a new mob. Each mob cost 40 score points. Max number of created mobs is 15.

You can also move the map. To do it, click on the map frame and drag it with the mouse

### Gameplay

![](images/doc3/gameplay.mp4)

### Music

[MenuTheme.mp3](images/doc3/MenuTheme.mp3)

[GameTheme.mp3](images/doc3/GameTheme.mp3)

[MenuTheme2.mp3](images/doc3/MenuTheme2.mp3)

[CharacterPreview.mp3](images/doc3/CharacterPreview.mp3)


