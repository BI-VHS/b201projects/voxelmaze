# Scenes ( Bioms )
- Sweet
- Egypt
- Neon
- Water

#### Notes
 <br/>
 Bioms content different amount of the same objects ( walls, floors, towrs etc )
 That's why bioms use different generation procedure.
 Every biome have towers, character and walls of the following types: T, X, I, -.
 <br/>
 Every biom looks like a square. The map is made of 4 squares connected to each other. The next step after process of bioms generetion is a creation of connections between them.
 Aftermath of those processes is a single maze separeted by bioms.
 <br/>

### Neon
Represents futuristic streets of neon city.

<img width="700" alt="portfolio_view" src="images/Neon/1.png">
<br/>There are number of walls but the only one type of floor. Neon is starting zone for robot Daniel.<br/>
<img width="300" alt="portfolio_view" src="images/Neon/2.png">
<br/>Neon tower looks like a pillar with a few circles aroung it and the gun/armor on the top.<br/>

### Underwater
Represents underwater landscape of maze.

<img width="700" alt="portfolio_view" src="images/Underwater/11.png">
<br/>There are number of walls but the only one type of floor. Underwater is starting zone for fish girl Alice.<br/>

<img width="300" alt="portfolio_view" src="images/Underwater/13.png">
<br/>Underwater tower looks like shipwreck decorated with corals and other stuff.<br/>


### Sugar
Represents fabulous world of sweets.

<img width="700" alt="portfolio_view" src="images/Sweet/21.png">
<br/>There is only one wall per type but floor is genereted by alg. to make it looks like a path. Sugar is starting zone for dreamer Elly.<br/>

<img width="300" alt="portfolio_view" src="images/Sweet/22.png">
<br/>Sweet tower looks like gingerbread house.<br/>

### Egypt
Represents ancient egyptian labyrint.

<img width="700" alt="portfolio_view" src="images/Egypt/31.png">
<br/>There are a few walls per type but floor is genereted by alg. to make it looks like a path. Egypt is starting zone for explorer Bob.<br/>

<img width="300" alt="portfolio_view" src="images/Egypt/33.png">
<br/>Egypt tower looks like ancient cat statue.<br/>


## More images

<img width="500" alt="portfolio_view" src="images/Egypt/34.png">

<img width="500" alt="portfolio_view" src="images/Neon/4.png">

<img width="500" alt="portfolio_view" src="images/Underwater/14.png">

<img width="500" alt="portfolio_view" src="images/Sweet/24.png">

<img width="500" alt="portfolio_view" src="images/Egypt/32.png">

<img width="500" alt="portfolio_view" src="images/Neon/3.png">

<img width="500" alt="portfolio_view" src="images/Underwater/12.png">

<img width="500" alt="portfolio_view" src="images/Sweet/23.png">

## Resume
 - 4 biomes
 - 4 unique hero and personal skills
 - A lot of objects in the scenes, like paints on the garages and etc.
 - 4 types of weapons


